FROM php:8.2-apache

RUN apt-get update && apt-get install -y \
    default-mysql-client \
    && docker-php-ext-install mysqli pdo pdo_mysql

WORKDIR /var/www/html
COPY . .

EXPOSE 80

CMD ["apache2-foreground"]

# docker build -t registry.gitlab.com/baptiste.bronsin/boulangere .
# docker push registry.gitlab.com/baptiste.bronsin/boulangere