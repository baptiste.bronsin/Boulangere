<?php

if(isset($_GET['like'])){
    $compteur = 1;
    $prb = false;
    while(!empty($_COOKIE['article'.$compteur])){
        if($_GET['like'] == $_COOKIE['article'.$compteur])
            $prb = true;
        $compteur++;
    }
    if(!$prb){
        setcookie(
            'article' . $compteur,
            $_GET['like'],
            [
                'expires' => time() + 365 * 24 * 3600,
                'secure' => true,
                'httponly' => true,
            ]
        );
    }
    header("location: index.php");
}

?>
<!doctype html>
<html lang="fr">
<head>
    <?php include_once("head/head.php"); ?>
    <title>Index</title>
    <style>
        #warningScreen{
            display: none;
        }

        .content{
            border-radius: 20px;
            padding: 4% 6%;
            margin: 4% 8%;
            text-align: center;
            z-index: 9;
            background-color: white;
            font-size: 100%;
        }

        @media screen and (max-width : 1140px) {
            #warningScreen{
                display: block;
                position: absolute;
                top: 0;
                bottom: 0;
                right: 0;
                left: 0;
                background-color: rgba(217, 217, 217, 0.90);
                z-index: 8;
                text-align: center;
                padding-top: 10%;
            }
        }
    </style>
</head>
<body>
<?php include_once("nav/nav.php"); ?>

<div id="warningScreen">
    <div class="content">
        <p style="font-weight: bold; text-align: center">
           Attention
        </p>
        <br>
        <p style="text-align: center">
            La taille de votre écran n'est pas assez large pour une utilisation optimale.
            Il se peut que le rendu visuel du site web ne corresponde pas aux attentes du développeur.<br>
            Désolé pour la gêne occasionnée.
        </p>
        <br>
        <a href="#" onclick="agree()" style="border: 1px dashed black; border-radius: 20px; padding: 2% 4%; color: black; text-decoration: none">J'ai compris</a>
    </div>
</div>

<h2>Promotions !</h2>
<h5>Vivez un réveillon de Noël magique</h5>

<?php
include_once("bdd.php");

$requete = $bdd->query('SELECT * FROM Produits');

while ($donnees = $requete->fetch()){
    if($donnees['promotion'] != 0){
        ?>
        <a <?= "href='article.php?article=".$donnees['idProduit']."'" ?> class="contenu">
            <div class="article row">
                <div class="col-sm-3 centre">
                    <h5><?= $donnees['nom']; ?></h5>
                    <img <?php echo "src='../donnees/img/". $donnees['photo']."'"; ?> class="recherche"/>
                </div>
                <div class="col-sm-5 description">
                    <p><?= $donnees['descriptif']; ?></p>
                </div>
                <div class="col-sm-2 centre prix">
                    <p>
                        <span class="texteBarre"><?= $donnees['prix']; ?></span>€
                        <br>
                        <span style="font-weight:bold"><?= $donnees['prix']*(100 - $donnees['promotion'])/100; ?>€</span>
                    </p>
                    <p>
                        <a <?= "href='index.php?like=".$donnees['idProduit']."'" ?>><i class='bx bxs-heart like'></i></a>
                    </p>
                </div>
                <div class="col-sm-2 centre promotion">
                    <p>
                        <span class="pourcentage">-<?= $donnees['promotion']; ?>%</span>
                    </p>
                </div>
            </div>
        </a>
        <hr>
        <?php
    }
}
$bdd = null;
?>

<script type="text/javascript">
    function agree(){
        document.getElementById("warningScreen").style.display = "none";
    }
</script>

</body>
</html>