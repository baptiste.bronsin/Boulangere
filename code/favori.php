<?php session_start();

if(isset($_GET['delete'])){
    $indice = $_GET['delete'];
    setcookie('article'.$indice);
    $indice++;
    while(!empty($_COOKIE['article'.$indice])){
        setcookie(
            'article'.($indice-1),
            $_COOKIE['article'.$indice],
            [
                'expires' => time() + 365 * 24 * 3600,
                'secure' => true,
                'httponly' => true,
            ]
        );
        setcookie('article'.$indice);
        $indice++;
    }
    header("location: favori.php");
}
?>
<!doctype html>
<html lang="fr">
<head>
    <?php include_once("head/head.php"); ?>
    <title>Mes favoris</title>
</head>
<body>
    <?php include_once("nav/nav.php"); ?>
    <h2>Mes favoris</h2>
    <?php
    include_once("bdd.php");

        if(empty($_COOKIE['article1'])){
            echo "<center><h4>Votre liste de favoris est actuellement vide</h4></center>";
        }
        else{
            $compteur = 1;
            while(!empty($_COOKIE['article'.$compteur])){
                $donnees = $bdd->query("select * from Produits where idProduit=".$_COOKIE['article'.$compteur])->fetch();
                ?>
                <div class="colle" style="padding: 10px 10px; float: left">
                    <a <?= "href='article.php?article=".$_COOKIE['article'.$compteur]."'" ?> style="color: black">
                        <div class="travaux colle centre" style="padding: 10px 10px; border-radius: 10px; height: 280px; width: 280px">
                            <?php
                            if(!$bdd->query("select * from Produits where idProduit=".$_COOKIE['article'.$compteur])->fetch()){
                                ?>
                                <h5>Produit Indisponible</h5>
                                <hr>
                                <?php
                            }
                            else{
                            ?>
                            <h5><?= $donnees['nom'] ?></h5>
                            <hr>
                            <img <?= "src='../donnees/img/".$donnees['photo']."'" ?> style="height: 200px">
                            <?php } ?>
                        </div>
                    </a>
                    <a <?= "href='favori.php?delete=".$compteur."'" ?>>
                        <div style="position: relative; top: -290px; left: 260px; background-color: #686868; color: white; border-radius: 16px; font-size: 20px; height: 30px; width: 30px; padding-left: 5px; padding-top: 1px" class="colle">
                            <i class='bx bx-message-square-x'></i>
                        </div>
                    </a>
                </div>
                <?php
                $compteur++;
            }
        }

    ?>
</body>
</html>
