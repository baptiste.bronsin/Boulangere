<?php

include_once ("mail.php");

session_start();

$bdd = null;

include_once ("bdd.php");

    // Lors de la connexion
    if(isset($_GET['connexion'])){
        if($bdd->query("select * from Clients where email='".$_POST['email']."' and bloque=1")->fetch())
            header("location: compte.php?compteBloque");
        elseif($bdd->query("select * from Clients where email='".$_POST['email']."' and active=0")->fetch())
            header("location: compte.php?compteSupp");
        else {
            $request = $bdd->query("SELECT email, motDePasse, prenom FROM Clients WHERE email='" . $_POST['email'] . "'");
            $donnees = $request->fetch();
            if (password_verify($_POST['password'], $donnees['motDePasse'])) {
                $_SESSION['email'] = $_POST['email'];
                $_SESSION['prenom'] = $donnees['prenom'];
                header("location: compte.php");
            } else {
                header("location: compte.php?error=connexion");
            }
        }
    }

    // Lors de l'inscription
    if(isset($_GET['email']) && !isset($_GET['verifieMdpOublie'])){

        // Cas où les deux mots de passes ne soient pas les mêmes
        if($_POST['password1'] != $_POST['password2'])
            header("location: compte.php?error=password");
        else {
            if($bdd->query("SELECT * FROM Clients WHERE email='".strtolower($_GET['email'])."' and active=0")->fetch()){
                $password = password_hash($_POST['password1'], PASSWORD_DEFAULT);
                $verifiedkey = md5(time());
                $bdd->exec("UPDATE Clients SET nom='".ucwords(strtolower($_GET['nom']))."' WHERE email='".strtolower($_GET['email'])."'");
                $bdd->exec("UPDATE Clients SET prenom='".ucwords(strtolower($_GET['prenom']))."' WHERE email='".strtolower($_GET['email'])."'");
                $bdd->exec("UPDATE Clients SET motDePasse='".$password."' WHERE email='".strtolower($_GET['email'])."'");
                $bdd->exec("UPDATE Clients SET vkey='".$verifiedkey."' WHERE email='".strtolower($_GET['email'])."'");
                $bdd->exec("UPDATE Clients SET admin=0 WHERE email='".strtolower($_GET['email'])."'");
                $bdd->exec("UPDATE Clients SET compteVerifie=0 WHERE email='".strtolower($_GET['email'])."'");
                $bdd->exec("UPDATE Clients SET creation=NOW() WHERE email='".strtolower($_GET['email'])."'");
                $bdd->exec("UPDATE Clients SET active=1 WHERE email='".strtolower($_GET['email'])."'");
            }
            else {
                $insert = $bdd->prepare("INSERT INTO Clients(nom, prenom, email, motDePasse, vkey) VALUES(:nom, :prenom, :email, :motDePasse, :vkey)");

                // On 'crypte' le mot de passe afin de le sauvegarder dans la BDD
                $password = password_hash($_POST['password1'], PASSWORD_DEFAULT);

                // On génère une clé aléatoire
                $verifiedkey = md5(time());

                // On insère les données das la BDD
                $insert->execute(array(
                    'nom' => ucwords(strtolower($_GET['nom'])),
                    'prenom' => ucwords(strtolower($_GET['prenom'])),
                    'email' => strtolower($_GET['email']),
                    'motDePasse' => $password,
                    'vkey' => $verifiedkey));
            }
            $_SESSION['email'] = strtolower($_GET['email']);
            $_SESSION['prenom'] = ucwords(strtolower($_GET['prenom']));

            if(envoyerMailValidation(ucwords(strtolower($_GET['nom'])), $_SESSION['prenom'], $_SESSION['email'], $verifiedkey)) {
                header("location: compte.php");
            }
            else {
                echo "<h2 style='text-align: center'>Un problème est survenu lors de l'envoie de l'email'.</h2>";
            }
        }
    }
    else if(isset($_GET['renvoieEmail'])){
        $request = $bdd->query("SELECT nom, email, vkey FROM Clients WHERE email='".$_SESSION['email']."'");
        $donnees = $request->fetch();
        if(envoyerMailValidation($donnees['nom'], $_SESSION['prenom'], $_SESSION['email'], $donnees['vkey'])) {
            header("location: compte.php");
        }
        else {
            echo "<h2 style='text-align: center'>Un problème est survenu lors de l'envoie de l'email'.</h2>";
        }
    }
    else if(isset($_GET['changeMdp'])){
        $request = $bdd->query("SELECT email, motDePasse FROM Clients WHERE email='".$_SESSION['email']."'");
        $donnees = $request->fetch();
        if(password_verify($_POST['mdpActuel'], $donnees['motDePasse'])){
            if($_POST['mdpNew1'] == $_POST['mdpNew2']){
                $password = password_hash($_POST['mdpNew1'], PASSWORD_DEFAULT);
                $bdd->exec("UPDATE Clients SET motDePasse='".$password."' WHERE email='".$_SESSION['email']."'");
                header("location: compte.php?changeMdp=&succes=true");
            }
            else
                header("location: compte.php?changeMdp=&error=mdpDiff");
        }
        else
            header("location: compte.php?changeMdp=&error=mdpActuel");
    }
    else if(isset($_GET['suppCompte'])){
        $request = $bdd->query("SELECT idCommande FROM Commandes WHERE email='".$_SESSION['email']."'");
        while($donnees = $request->fetch()){
            $bdd->exec("DELETE FROM LignesCommandes WHERE idCommande='".$donnees['idCommande']."'");
        }
        $bdd->exec("DELETE FROM Commandes WHERE email='".$_SESSION['email']."'");
        $bdd->exec("UPDATE Clients SET active=0 WHERE email='".$_SESSION['email']."'");
        $bdd->exec("UPDATE Clients SET motDePasse='' WHERE email='".$_SESSION['email']."'");

        $prenom = $_SESSION['prenom'];

        $message = "<head>
                    <style>
                        body{
                            margin: 10px 10px;
                        }
                    </style>
                    </head>
                    <body>
                        <h2>Bonjour $prenom,</h2>
                        <p>Suite à votre demande, nous vous confirmons que votre compte vient d'être supprimé !<br>Nous espérons vous revoir très bientôt :/</p>
                        <p>Bien cordialement, <br>L'équipe Boulangère</p>
                    </body>";

        if(EnvoyerMail($_SESSION['email'], "", $prenom, "Suppression du compte", $message)) {
            session_destroy();
            header("location: compte.php?aurevoir");
        }
        else {
            echo "<h2 style='text-align: center'>Un problème est survenu lors de la suppression de votre compte.</h2>";
        }
    }
    else if(isset($_GET['mdpOublie'])){
        $request = $bdd->query("SELECT COUNT(*) AS estPresent FROM Clients WHERE email='".$_POST['email']."' ");
        $donnees = $request->fetch();
        if($donnees['estPresent'] == 0) {
            header("location: compte.php?mdpOublie=true&error=mdpOublie");
        }
        else {
            $request = $bdd->query("SELECT nom, prenom, email, vkey FROM Clients WHERE email='" . $_POST['email'] . "'");
            $donnees = $request->fetch();

            if(envoyerMailResetMDP($donnees['nom'], $donnees['prenom'], $_POST['email'],  $donnees['vkey'])){
                header("location: compte.php");
            }
            else {
                echo "<h2 style='text-align: center'>Un problème est survenu lors de la réinitialisation de votre mot de passe.</h2>";
            }
        }
    }
    else if(isset($_GET['verifieMdpOublie'])){
        if($_POST['mdpNew1'] == $_POST['mdpNew2']){
            $password = password_hash($_POST['mdpNew1'], PASSWORD_DEFAULT);
            $verite = $bdd->exec("UPDATE Clients SET motDePasse='".$password."' WHERE email='".$_GET['email']."'");
            $request = $bdd->query("SELECT prenom FROM Clients WHERE email='".$_GET['email']."'");
            $donnees = $request->fetch();
            $_SESSION['email'] = $_GET['email'];
            $_SESSION['prenom'] = $donnees['prenom'];
            header("location: compte.php");
        }
        else{
            header("location: verify.php?email=".$_GET['email']."&vkey=".$_GET['vkey']."&mdpOublie=true&error");
        }
    }



    function envoyerMailValidation($nom, $prenom, $email, $vkey){
        $message = "<head>
                        <style>
                            body{
                                margin: 10px 10px;
                            }
                        </style>
                    </head>
                    <body>
                        <h2>Bonjour $prenom,</h2>
                        <p>Nous sommes heureux de vous compter parmis nous.<br>Afin de pouvoir continuer vos achats sur notre site, veuillez <a href='http://Baraly.fr/Boulangere/ProjetFac/code/verify.php?email=$email&vkey=$vkey'>valider votre compte</a>.</p>
                        <p>Bien cordialement, <br>L'équipe Boulangère</p>
                    </body>";

        return EnvoyerMail($email, $nom, $prenom, "Email de vérification", $message);
    }

    function envoyerMailResetMDP($nom, $prenom, $email, $vkey){
        $message = "<head>
                            <style>
                                body{
                                    margin: 10px 10px;
                                }
                            </style>
                        </head>
                        <body>
                            <h2>Bonjour $prenom,</h2>
                            <p>Si vous souhaitez réinitialiser votre mot de passe, veuillez cliquer sur ce <a href='http://Baraly.fr/Boulangere/ProjetFac/code/verify.php?email=" . $email . "&vkey=" . $vkey . "&mdpOublie'>lien</a>.</p>
                            <hp>Bien cordialement, <br>L'équipe Boulangère</p>
                        </body>";

        return EnvoyerMail($email, $nom, $prenom, "Mot de passe oublié", $message);
    }

$bdd = null;
?>