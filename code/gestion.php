<?php session_start(); ?>
<!doctype html>
<html lang="fr">
<head>
    <?php include_once("head/head.php"); ?>
    <title>Gestion - Article</title>
    <style>
        .commandes{
            display: grid;
            grid-template-columns: 3fr 2fr;
        }
        input[type='submit']{
            background: royalblue;
            border-radius: 10px;
            border: none;
            color: white;
            padding: 2px 10px;
            cursor: pointer;
        }
        .article2{
            float: left;
            width: 150px;
            height: 110px;
            border-radius: 10px;
            background-color: #363636;
            padding: 6px 10px;
            text-align: center;
            margin: 10px 14px;
        }
        .image{
            background-color: white;
            border-radius: 5px;
            text-align: center;
            width: 100px;
            padding: 2px 4px;
            display: inline-block;
        }
        .image img{
            width: 50px;
            height: 100%;
        }
    </style>
</head>
<body>
<?php include_once("nav/nav.php"); ?>

<h2>Gestion administrateur</h2>

<?php
    include_once("bdd.php");

    if(!isset($_SESSION['email'])){
        echo "<enter style='text-align: center; margin: 0 10px'><h4>Veuillez vous connecter afin d'avoir accès à cette page<br><a href='compte.php'>Me connecter</a></h4></enter>";
    }
    elseif($bdd->query("select * from Clients where email='".$_SESSION['email']."' and admin=0")->fetch()){
        echo "<enter style='text-align: center; margin: 0 10px'><h4>Vous n'avez pas les autorisations pour pouvoir accéder à cette page !</h4><a href='gestionPost.php?requestAdmin=".$_SESSION['email']."'><h4>Demander l'accès</h4></a></enter>";
        if(isset($_GET['demandeSent'])){
            echo "<center><h4>Votre demande vient d'être envoyée !</h4></center>";
        }
    }
    else{
        ?>
        <div class="listeActionArticle" style="height: 240px; width: 200px; margin-top: 30px">
            <ul>
                <?php
                if(isset($_GET['user'])){
                    echo "<li><a href='gestion.php?user' style='background-color: white; border-radius: 5px; color: black; padding: 2px 5px'>Utilisateurs</a></li>";
                }
                else{
                    echo "<li><a href='gestion.php?user'>Utilisateurs</a></li>";
                }

                if(isset($_GET['article'])){
                    echo "<li><a href='gestion.php?article' style='background-color: white; border-radius: 5px; color: black; padding: 2px 5px'>Articles</a></li>";
                }
                else{
                    echo "<li><a href='gestion.php?article'>Articles</a></li>";
                }

                if(isset($_GET['stat'])){
                    echo "<li><a href='gestion.php?stat' style='background-color: white; border-radius: 5px; color: black; padding: 2px 5px'>Statistiques</a></li>";
                }
                else{
                    echo "<li><a href='gestion.php?stat'>Statistiques</a></li>";
                }

                if(isset($_GET['commandes'])){
                    echo "<li><a href='gestion.php?commandes' style='background-color: white; border-radius: 5px; color: black; padding: 2px 5px'>Commandes</a></li>";
                }
                else{
                    echo "<li><a href='gestion.php?commandes'>Commandes</a></li>";
                }

                if(isset($_GET['mails'])){
                    echo "<li><a href='gestion.php?mails' style='background-color: white; border-radius: 5px; color: black; padding: 2px 5px'>Mails</a></li>";
                }
                else{
                    echo "<li><a href='gestion.php?mails'>Mails</a></li>";
                }

                if(isset($_GET['requests'])){
                    echo "<li><a href='gestion.php?requests' style='background-color: white; border-radius: 5px; color: black; padding: 2px 5px'>Requêtes</a></li>";
                }
                else{
                    echo "<li><a href='gestion.php?requests'>Requêtes</a></li>";
                }
                ?>
            </ul>
        </div>
        <div class="admin">
            <?php
                if(isset($_GET['user'])){
                    if(empty($_GET['user'])){
                    ?>
                        <form action="gestion.php?user" method="post">
                            <label>Nom : <input type="text" name="nom"></label>
                            <label>Prénom : <input type="text" name="prenom"></label>
                            <input type="submit" value="Rechercher">
                        </form>
                        <hr>
                    <div style="position: absolute; top: 70px; bottom: 0px; right: 20px; left: 20px; overflow: auto">
                        <?php
                        if(!empty($_POST['nom']) and empty($_POST['prenom']))
                            $request = $bdd->query("select * from Clients where nom='".$_POST['nom']."'");
                        else if(!empty($_POST['prenom']) and empty($_POST['nom']))
                            $request = $bdd->query("select * from Clients where prenom='".$_POST['prenom']."'");
                        else if (!empty($_POST['prenom']) and !empty($_POST['nom']))
                            $request = $bdd->query("select * from Clients where nom='".$_POST['nom']."' and prenom='".$_POST['prenom']."'");
                        else
                            $request = $bdd->query("select * from Clients");
                        $prb = true;
                            while($donnees = $request->fetch()){
                                $prb = false;
                                ?>
                                <a <?= "href='gestion.php?user=".$donnees['email']."'" ?>>
                                <div class="row affichageClient">
                                    <div class="col-sm-3 centre">
                                        <?= strtoupper($donnees['nom']) ?>
                                        <?= $donnees['prenom'] ?>
                                    </div>
                                    <div class="col-sm-5 centre">
                                        <?= $donnees['email'] ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                            if($donnees['compteVerifie'] == 1)
                                                echo "Compte vérifié !";
                                            else
                                                echo "Compte en attente";

                                            if($donnees['active'] == 0)
                                                echo " (Désactivé)";
                                        ?>
                                    </div>
                                </div>
                                </a>
                                <?php
                            }
                            if($prb){
                                echo "<center><h3>Nous ne trouvons aucunes correspondances</h3></center>";
                            }
                    }
                    else{
                        $donnees = $bdd->query("select * from Clients where email='".$_GET['user']."'")->fetch();
                        ?>
                            <div class="colle">
                                <h5>Nom : <?= $donnees['nom'] ?></h5>
                                <h5>Prénom : <?= $donnees['prenom'] ?></h5>
                                <h5>Email : <?= $donnees['email'] ?></h5>
                                <h5>Téléphone : +33<?= $donnees['telephone'] ?></h5>
                                <h5>Adresse : <?= $donnees['adresse'] ?></h5>
                            </div>
                            <div style="margin-top: 80px" class="colle">
                                <h5>Ce compte a été créé le : <?= date('d M Y', strtotime($donnees['creation'])) ?></h5>
                                <?php
                                    if($donnees['compteVerifie'] == 0)
                                        echo "<h5 style='color:indianred'>Ce compte n'a pas été vérifié !</h5>";
                                    else
                                        echo "<h5 style='color: green'>Ce compte a été vérifié !</h5>";

                                    if($donnees['bloque'] == 1)
                                        echo "<h5 style='background-color: #FB9883; padding: 2px 5px; border-radius: 10px' class='colle'>Ce compte a été bloqué</h5>";

                                    if($donnees['active'] == 0)
                                        echo "<h5 style='background-color: #FB9883; padding: 2px 5px; border-radius: 10px' class='colle'>Ce compte a été désactivé</h5>";
                                ?>
                                <h5>Clé de sécurité : <?= $donnees['vkey'] ?></h5>
                                <?php
                                    if($donnees['admin'] == 1)
                                        echo "<h5 style='background-color: #363636; color: white; padding: 2px 6px; border-radius: 8px' class='colle'>Cet utilisateur est administrateur</h5>";
                                ?>
                            </div>
                            <div class="travaux" style="position: absolute; top: 10px; bottom: 10px; right: 24px; width: 40%; border-radius: 10px; padding: 10px 30px">
                                <div class="row">
                                    <?php
                                        $nbCommande = $bdd->query("select count(*) as nb from Commandes where email='".$_GET['user']."' and etat<>'Panier'")->fetch();
                                        $moyenne = $bdd->query("select avg(montant) as moyenne from Commandes, LignesCommandes where Commandes.idCommande = LignesCommandes.idCommande and email='".$_GET['user']."' and etat<>'Panier'")->fetch();
                                        $nbProduits = $bdd->query("select sum(quantite) as nbProduit from Commandes, LignesCommandes where Commandes.idCommande = LignesCommandes.idCommande and email='".$_GET['user']."' and etat<>'Panier'")->fetch();
                                        $total = $bdd->query("select sum(montant) as total from Commandes, LignesCommandes where Commandes.idCommande = LignesCommandes.idCommande and email='".$_GET['user']."' and etat<>'Panier'")->fetch();
                                    ?>
                                    <div class="centre col-sm-5 travaux"><h5>Nombre de commandes</h5><hr><?= $nbCommande['nb'] ?></div>
                                    <div class="col-sm-2"></div>
                                    <div class="centre col-sm-5 travaux"><h5>Montant moyen</h5><hr><?= 0 + $moyenne['moyenne'] ?>€</div>
                                    <div class="centre col-sm-5 travaux" style="margin-top: 20px"><h5>Quantité commandé</h5><hr><?= 0 + $nbProduits['nbProduit'] ?></div>
                                    <div class="col-sm-2"></div>
                                    <div class="centre col-sm-5 travaux" style="margin-top: 20px"><h5>Montant total</h5><hr><?= 0 + $total['total'] ?>€</div>
                                </div>
                                <div style="position: absolute; bottom: 10px; top: 260px; overflow: auto; padding: 20px 20px">
                                    <a <?= "href='gestionPost.php?user=".$_GET['user']."&changerVkey'" ?> class="button" style="display: inline-block;">Changer la clé de sécurité</><br>
                                    <a <?= "href='gestionPost.php?user=".$_GET['user']."&validerCompte'" ?> class="button" style="display: inline-block; margin-top: 10px">Email : Valider le compte</a><br>
                                    <a <?= "href='gestionPost.php?user=".$_GET['user']."&changeMdp'" ?> class="button" style="display: inline-block; margin-top: 10px">Email : Changer le mot de passe</a><br>
                                    <?php
                                    $donnees = $bdd->query("select * from Clients where email='".$_GET['user']."'")->fetch();
                                        if($donnees['bloque'] == 1)
                                            echo "<a href='gestionPost.php?user=".$_GET['user']."&debloquer' class='button' style='display: inline-block; margin-top: 10px'>Débloquer le compte</a><br>";
                                        else
                                            echo "<a href='gestionPost.php?user=".$_GET['user']."&bloquer' class='button' style='display: inline-block; margin-top: 10px'>Bloquer le compte</a><br>";

                                        if($donnees['active'] == 1)
                                            echo "<a href='gestionPost.php?user=".$_GET['user']."&suppCompte' class='button' style='display: inline-block; margin-top: 10px'>Désactiver le compte</a><br>";
                                        else
                                            echo "<a href='gestionPost.php?user=".$_GET['user']."&activerCompte' class='button' style='display: inline-block; margin-top: 10px'>Activer le compte</a><br>";
                                        ?>

                                </div>
                            </div>
                        <?php
                    }
                        ?>
                    </div>
                    <?php
                }
                elseif(isset($_GET['article'])){
                    if(!empty($_GET['article'])){
                        $donnees = $bdd->query("SELECT * FROM Produits WHERE idProduit = " . $_GET['article'])->fetch();
                    ?>
                            <div style="display: grid; grid-template-columns: 2fr 1fr">
                                <div>
                                    <form action="gestionPost.php?modifierProduit=<?= $_GET['article'] ?>" method="POST">
                                        <h5>Id : <?= $_GET['article'] ?></h5>
                                        <label>Nom : <input type="text" name="nom" value="<?= $donnees['nom'] ?>"></label><br>
                                        <label>Marque : <input type="text" name="marque" value="<?= $donnees['marque'] ?>"></label><br>
                                        <label>Catégorie : <input type="text" name="categorie" value="<?= $donnees['categorie'] ?>"></label><br>
                                        <label>Descriptif : <textarea name="descriptif"><?= $donnees['descriptif'] ?></textarea></label><br>
                                        <label>Description : <textarea name="description"><?= $donnees['description'] ?></textarea></label><br>
                                        <label>Prix : <input type="number" name="prix" value="<?= $donnees['prix'] ?>"></label><br>
                                        <label>Stock : <input type="number" name="stock" value="<?= $donnees['stock'] ?>"></label><br>
                                        <label>Promotion : <input type="number" name="promotion" value="<?= $donnees['promotion'] ?>"></label><br>
                                        <input type="submit" value="Mettre à jour">
                                    </form>
                                </div>
                                <div style="position: relative">
                                    <h5>Dernières transactions</h5>
                                    <ul style="position: absolute; top: 20px; bottom: 10px; left: 10px; overflow: auto">
                                        <?php
                                        $transactions = $bdd->query("SELECT Clients.email as email, LignesCommandes.quantite as quantite, Commandes.date_ as jour FROM Clients, Commandes, LignesCommandes WHERE Clients.email = Commandes.email AND Commandes.idCommande = LignesCommandes.idCommande AND LignesCommandes.idProduit = " . $_GET['article']);
                                        while($infos = $transactions->fetch()){
                                            echo "<li>Client (<a href='gestion.php?user=".$infos['email']."'>infos</a>) x ".$infos['quantite']." le ".date("d/m/Y", strtotime($infos['jour']))."</li>";
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        <?php
                    }
                    else{
                        ?>
                        <form action="gestion.php?article" method="POST">
                            <label>
                                Produit :
                                <?php
                                if(!empty($_POST['nomProduit'])){
                                    echo "<input type='text' name='nomProduit' value='".$_POST['nomProduit']."'>";
                                }
                                else{
                                    echo "<input type='text' name='nomProduit'>";
                                }
                                ?>
                            </label>
                            <label>
                                Marque :
                                <?php
                                if(!empty($_POST['marque'])){
                                    echo "<input type='text' name='marque' value='".$_POST['marque']."'>";
                                }
                                else{
                                    echo "<input type='text' name='marque'>";
                                }
                                ?>
                            </label>
                            <label>
                                Catégorie :
                                <?php
                                if(!empty($_POST['categorie'])){
                                    echo "<input type='text' name='categorie' value='".$_POST['categorie']."'>";
                                }
                                else{
                                    echo "<input type='text' name='categorie'>";
                                }
                                ?>
                            </label>
                            <label>
                                Promotion :
                                <?php
                                if(!empty($_POST['promotion'])){
                                    echo "<input type='checkbox' name='promotion' checked>";
                                }
                                else{
                                    echo "<input type='checkbox' name='promotion'>";
                                }
                                ?>
                            </label>
                            <input type="submit" value="Rechercher">
                        </form>
                        <hr>
                        <?php
                            $sql = "SELECT * FROM Produits";

                            $where = false;

                            if(!empty($_POST['nomProduit'])){
                                $sql .= " WHERE nom LIKE '".$_POST['nomProduit']."'";
                                $where = true;
                            }
                            elseif (!empty($_POST['marque'])){
                                if(!$where){
                                    $sql .= " WHERE marque = '".$_POST['marque']."'";
                                    $where = true;
                                }
                                else{
                                    $sql .= " AND marque = '".$_POST['marque']."'";
                                }
                            }
                            elseif (!empty($_POST['categorie'])){
                                if(!$where){
                                    $sql .= " WHERE categorie LIKE '".$_POST['categorie']."'";
                                    $where = true;
                                }
                                else{
                                    $sql .= " AND categorie LIKE '".$_POST['categorie']."'";
                                }
                            }
                            elseif (!empty($_POST['promotion'])){
                                if(!$where){
                                    $sql .= " WHERE promotion > 0";
                                }
                                else{
                                    $sql .= " AND promotion > 0";
                                }
                            }

                            $donnees = $bdd->query($sql);

                            ?>
                        <div style="position: absolute; top: 70px; bottom: 0px; right: 20px; left: 20px; overflow: auto">
                            <?php
                            while($produit = $donnees->fetch()){
                            ?>
                                    <a href="gestion.php?article=<?= $produit['idProduit'] ?>">
                                        <div class="article2">
                                            <div class="image">
                                                <img src="../donnees/img/<?= $produit['photo'] ?>">
                                            </div>
                                            <div style="text-align: center">
                                                <p style="color: white; display: inline-block;"><?= $produit['nom'] ?></p>
                                            </div>
                                        </div>
                                    </a>
                            <?php
                            }
                            ?>
                        </div>
                            <?php
                    }
                    ?>
                    <?php
                }
                elseif(isset($_GET['stat'])){
                    ?>
                    <?php
                }
                elseif(isset($_GET['commandes'])){
                    if(!empty($_GET['commandes'])){
                        $donnees = $bdd->query("SELECT * FROM Clients, Commandes WHERE Clients.email = Commandes.email AND idCommande= " . $_GET['commandes'])->fetch();
                        $nbArticles = $bdd->query("SELECT SUM(quantite) AS nombre FROM LignesCommandes WHERE idCommande = " . $_GET['commandes'])->fetch();
                        $montant = $bdd->query("SELECT SUM(montant) AS montant FROM LignesCommandes WHERE idCommande = " . $_GET['commandes'])->fetch();
                        $listArticle = $bdd->query("SELECT * FROM Produits, LignesCommandes WHERE Produits.idProduit = LignesCommandes.idProduit AND idCommande = " . $_GET['commandes']);

                        ?>
                    <div class="commandes">
                        <div>
                            <h5>Client : <?= $donnees['nom'] ?> <?= $donnees['prenom'] ?> <a href="gestion.php?user=<?= $donnees['email'] ?>">infos</a></h5>
                            <h5>Numéro de commande : <?= $_GET['commandes'] ?></h5>
                            <h5>Nombre d'articles commandés : <?= $nbArticles['nombre'] ?></h5>
                            <h5>Commandé le : <?= date('d M Y', strtotime($donnees['date_'])) ?></h5>
                            <h5>Prix : <?= $montant['montant'] ?>€</h5>
                            <form action="gestionPost.php?idCommande=<?= $_GET['commandes'] ?>&changerEtat" method="POST">
                            <h5>Status :
                                <select name="status">
                                    <?php
                                        $status = ['validation', 'préparation', 'livraison', 'réceptionnée'];
                                        for($i = 0; $i < count($status); $i++){
                                            if ($status[$i] == $donnees['etat']){
                                                echo "<option value='".$status[$i]."' selected>".$status[$i]."</option>";
                                            }
                                            else{
                                                echo "<option value='".$status[$i]."'>".$status[$i]."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                                <input type="submit" value="Mettre à jour">
                            </h5>
                            </form>
                        </div>
                        <div>
                            <h5>Liste des articles</h5>
                            <ul>
                                <?php
                                while($article = $listArticle->fetch()){
                                    ?>
                                    <li><?= $article['nom'] ?> ( <?= $article['quantite'] ?> fois )</li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                        <?php

                    }
                    else{
                        ?>
                            <form action="gestion.php?commandes" method="POST">
                                <?php
                                if(isset($_POST['validation'])){
                                    echo "<label>Validation <input type='checkbox' name='validation' checked></label> | ";
                                }
                                else{
                                    echo "<label>Validation <input type='checkbox' name='validation'></label> | ";
                                }
                                if(isset($_POST['préparation'])){
                                    echo "<label>Préparation <input type='checkbox' name='préparation' checked></label> | ";
                                }
                                else{
                                    echo "<label>Préparation <input type='checkbox' name='préparation'></label> | ";
                                }
                                if(isset($_POST['livraison'])){
                                    echo "<label>Livraison <input type='checkbox' name='livraison' checked></label> | ";
                                }
                                else{
                                    echo "<label>Livraison <input type='checkbox' name='livraison'></label> | ";
                                }
                                if(isset($_POST['réceptionnée'])){
                                    echo "<label>Réceptionnée <input type='checkbox' name='réceptionnée' checked></label> | ";
                                }
                                else{
                                    echo "<label>Réceptionnée <input type='checkbox' name='réceptionnée'></label> | ";
                                }
                                if(isset($_POST['recherche']))
                                    echo "<label>Rechercher <input type='text' name='recherche' value='".$_POST['recherche']."'></label>";
                                else
                                    echo "<label>Rechercher <input type='text' name='recherche'></label>";
                                ?>
                                <input type="submit">
                            </form>
                        <hr>
                        <div style="overflow: auto">
                        <?php
                        if(isset($_POST['validation']) or !isset($_POST['validation']) and !isset($_POST['préparation']) and !isset($_POST['livraison']) and !isset($_POST['réceptionnée'])){
                            if(!empty($_POST['recherche'])){
                                if((int)$_POST['recherche'] != 0)
                                    $request = $bdd->query("select * from Clients, Commandes where Clients.email = Commandes.email and idCommande=".$_POST['recherche']." and etat='validation'");
                                else
                                    $request = $bdd->query("select * from Clients, Commandes where Clients.email = Commandes.email and (nom='".$_POST['recherche']."' or prenom='".$_POST['recherche']."') and etat='validation'");
                            }
                            else{
                                $request = $bdd->query("select * from Clients, Commandes where Clients.email = Commandes.email and etat='validation'");
                            }
                            ?>
                            <h3>Commandes en cours de validation</h3>
                            <?php
                            while($donnees = $request->fetch()){
                                ?>
                                    <div class="travaux" style="padding-top: 8px; padding-left: 8px; border-radius: 10px; margin-bottom: 10px">
                                        <h5>Nom : <?= $donnees['nom'] ?> | Prénom : <?= $donnees['prenom'] ?><br>Numéro de commande : <?= $donnees['idCommande'] ?> | Commandé le : <?= date('d M Y', strtotime($donnees['date_'])) ?></h5>
                                        <?php echo "<a href='gestion.php?commandes=".$donnees['idCommande']."' style='color: black'><i class='bx bx-receipt' style='position: absolute; right: 20px; font-size: 50px; margin-top: -58px'></i></a>"; ?>
                                    </div>
                                <?php
                            }
                        }
                        if(isset($_POST['preparation']) or !isset($_POST['validation']) and !isset($_POST['préparation']) and !isset($_POST['livraison']) and !isset($_POST['réceptionnée'])){
                            if(!empty($_POST['recherche'])){
                                if((int)$_POST['recherche'] != 0)
                                    $request = $bdd->query("select * from Clients, Commandes where Clients.email = Commandes.email and idCommande=".$_POST['recherche']." and etat='préparation'");
                                else
                                    $request = $bdd->query("select * from Clients, Commandes where Clients.email = Commandes.email and (nom='".$_POST['recherche']."' or prenom='".$_POST['recherche']."') and etat='préparation'");
                            }
                            else{
                                $request = $bdd->query("select * from Clients, Commandes where Clients.email = Commandes.email and etat='préparation'");
                            }
                            ?>
                            <h3>Commandes en cours de préparation</h3>
                            <?php
                            while($donnees = $request->fetch()){
                                ?>
                                <div class="travaux" style="padding-top: 8px; padding-left: 8px; border-radius: 10px; margin-bottom: 10px">
                                    <h5>Nom : <?= $donnees['nom'] ?> | Prénom : <?= $donnees['prenom'] ?><br>Numéro de commande : <?= $donnees['idCommande'] ?> | Commandé le : <?= date('d M Y', strtotime($donnees['date_'])) ?></h5>
                                    <?php echo "<a href='gestion.php?commandes=".$donnees['idCommande']."' style='color: black'><i class='bx bx-receipt' style='position: absolute; right: 20px; font-size: 50px; margin-top: -58px'></i></a>"; ?>
                                </div>
                                <?php
                            }
                        }
                        if(isset($_POST['livraison']) or !isset($_POST['validation']) and !isset($_POST['préparation']) and !isset($_POST['livraison']) and !isset($_POST['réceptionnée'])){
                            if(!empty($_POST['recherche'])){
                                if((int)$_POST['recherche'] != 0)
                                    $request = $bdd->query("select * from Clients, Commandes where Clients.email = Commandes.email and idCommande=".$_POST['recherche']." and etat='livraison'");
                                else
                                    $request = $bdd->query("select * from Clients, Commandes where Clients.email = Commandes.email and (nom='".$_POST['recherche']."' or prenom='".$_POST['recherche']."') and etat='livraison'");
                            }
                            else{
                                $request = $bdd->query("select * from Clients, Commandes where Clients.email = Commandes.email and etat='livraison'");
                            }
                            ?>
                            <h3>Commandes en cours de livraison</h3>
                            <?php
                            while($donnees = $request->fetch()){
                                ?>
                                <div class="travaux" style="padding-top: 8px; padding-left: 8px; border-radius: 10px; margin-bottom: 10px">
                                    <h5>Nom : <?= $donnees['nom'] ?> | Prénom : <?= $donnees['prenom'] ?><br>Numéro de commande : <?= $donnees['idCommande'] ?> | Commandé le : <?= date('d M Y', strtotime($donnees['date_'])) ?></h5>
                                    <?php echo "<a href='gestion.php?commandes=".$donnees['idCommande']."' style='color: black'><i class='bx bx-receipt' style='position: absolute; right: 20px; font-size: 50px; margin-top: -58px'></i></a>"; ?>
                                </div>
                                <?php
                            }
                        }
                        if(isset($_POST['réceptionnée']) or !isset($_POST['validation']) and !isset($_POST['préparation']) and !isset($_POST['livraison']) and !isset($_POST['réceptionnée'])){
                            if(!empty($_POST['recherche'])){
                                if((int)$_POST['recherche'] != 0)
                                    $request = $bdd->query("select * from Clients, Commandes where Clients.email = Commandes.email and idCommande=".$_POST['recherche']." and etat='réceptionnée'");
                                else
                                    $request = $bdd->query("select * from Clients, Commandes where Clients.email = Commandes.email and (nom='".$_POST['recherche']."' or prenom='".$_POST['recherche']."') and etat='réceptionnée'");
                            }
                            else{
                                $request = $bdd->query("select * from Clients, Commandes where Clients.email = Commandes.email and etat='réceptionnée'");
                            }
                            ?>
                            <h3>Commandes réceptionnées</h3>
                            <?php
                            while($donnees = $request->fetch()){
                                ?>
                                <div class="travaux" style="padding-top: 8px; padding-left: 8px; border-radius: 10px; margin-bottom: 10px">
                                    <h5>Nom : <?= $donnees['nom'] ?> | Prénom : <?= $donnees['prenom'] ?><br>Numéro de commande : <?= $donnees['idCommande'] ?> | Commandé le : <?= date('d M Y', strtotime($donnees['date_'])) ?></h5>
                                    <?php echo "<a href='gestion.php?commandes=".$donnees['idCommande']."' style='color: black'><i class='bx bx-receipt' style='position: absolute; right: 20px; font-size: 50px; margin-top: -58px'></i></a>"; ?>
                                </div>
                                <?php
                            }
                        }
                        echo "</div>";
                    }
                }
                elseif(isset($_GET['mails'])){
                    ?>
                    <?php
                }
                elseif(isset($_GET['requests'])){
                    $request = $bdd->query("SELECT * FROM Requete, Clients WHERE Requete.email = Clients.email AND resolu=0");
                    while($donnees = $request->fetch()){
                        ?>
                            <div class="row affichageClient" style="padding-top: 10px">
                                <div class="col-sm-6 centre">
                                    <h5>Message provenant de : <?= $donnees['nom'] ?> <?= $donnees['prenom'] ?></h5>
                                </div>
                                <div class="col-sm-4 centre">
                                    <h5>Envoyé le : <?= date('d M Y', strtotime($donnees['date_'])) ?></h5>
                                </div>
                                <div class="col-sm-2">
                                    <a href="#" style="color: white; position: absolute; right: 10px; top: -12px; font-size: 40px"><i class='bx bx-list-ul'></i></a>
                                </div>
                            </div>
                        <?php
                    }
                }
                else{
                    echo "<center style='margin-top: 100px'><h4>Veuillez sélectionner une section</h4></center>";
                }
            ?>
        </div>
        <?php
    }
?>
</body>
</html>